import Toast from "./Toast.vue";
const MyPlugin = {};
MyPlugin.install = function(Vue) {
  //创建组件构造器
  let ToastContrustor = Vue.extend(Toast);
  //创建一个组件
  let toast = new ToastContrustor();
  //将组建挂载到元素上
  toast.$mount(document.createElement("div"));
  //插入到body中
  document.body.appendChild(toast.$el);
  //挂载到Vue原型上
  Vue.prototype.$toast = toast;
};
export default MyPlugin;