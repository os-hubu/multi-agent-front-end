import $ from "jquery";
import Vue from "vue";

export function sendMessageInitData(form) {
  console.log(form);
  form.type = "updateSetting";
  //口罩率，疫苗率，感染者的口罩率，感染者的疫苗率，无症状口罩率，无症状疫苗率，总数，感染者人数，无症状人数
  // let data = { "type": "updateSetting", "agentMasksratio": 40, "agentVaccineratio": 50, "infectedagenMasksratio": 10,
  // "infectedagenVaccineratio": 10, "asymptomaticagentMasksratio": 10,
  // "asymptomaticagentVaccineratio": 10, "agentNumber": `${num}`,
  // "infectedagentNumber": "13", "asymptomaticagentNumber": "13" }
  $.ajax({
    url: "/api1",
    data: JSON.stringify(form),
    type: "post",
    dataType: "json",
    contentType: "application/json",
    // async:false,
    success: function (data) {
      if (data.msg == "success") {
        new Vue().$toast.show("初始化成功");
        console.log(data)
      }
    },
    error: function () {
      new Vue().$toast.show("初始化失败");
    },
  });
}

export function resumeRunAgent() {
  let data = { type: "resumeRunAgent" };
  $.ajax({
    url: "/api1",
    data: JSON.stringify(data),
    type: "post",
    dataType: "json",
    contentType: "application/json",
    success: function (data) {
      if (data.msg == "success") {
        new Vue().$toast.show("结束暂停成功");
        console.log(data)
      }
    },
  });
}

export function stopAgent() {
  let data = { type: "stopAgent" };
  $.ajax({
    url: "/api1",
    data: JSON.stringify(data),
    type: "post",
    dataType: "json",
    contentType: "application/json",
    success: function (data) {
      if (data.msg == "success") {
        console.log(data)
        new Vue().$toast.show("暂停成功");
      }
    },
  });
}

export function endAgent() {
  let data = { type: "endAgent" };
  $.ajax({
    url: "/api1",
    data: JSON.stringify(data),
    type: "post",
    dataType: "json",
    contentType: "application/json",
    success: function (data) {
      if (data.msg == "success") {
        console.log(data)
        new Vue().$toast.show("终止成功");
      }
    },
  });
}

export function startAgent() {
  let data = { type: "startAgent" };
  $.ajax({
    url: "/api1",
    data: JSON.stringify(data),
    type: "post",
    dataType: "json",
    contentType: "application/json",
    success: function (data) {
      if (data.msg == "success") {
        console.log(data)
        new Vue().$toast.show("开启成功");
      }
    },
  });
}

export function reLoadAgent() {
  let data = { type: "reLoadAgent" };
  $.ajax({
    url: "/api1",
    data: JSON.stringify(data),
    type: "post",
    dataType: "json",
    contentType: "application/json",
    success: function (data) {
      console.log(data)
      // if (data.msg == "success") {
      //   // new Vue().$toast.show("重置成功");
      // }
    },
  });
}
