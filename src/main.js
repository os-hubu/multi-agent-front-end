import Vue from 'vue'
import App from './App.vue'

import store from './store'


import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Toast from "./components/Toast/index";
import VueRouter from 'vue-router';
Vue.use(Toast);

Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$bus=new Vue();


new Vue({

  store,
  render: h => h(App)
}).$mount('#app')
