<h1 align="center">多智能体</h1>

<div align="center">

新冠疫情时空传播过程可视化模拟

</div>

"多智能体"项目选取武汉市硚口区作为实验区域，进行了多场景仿真实验，模拟了时空演化、戴口罩率、疫苗接种率、医疗条件等因素对新冠疫情传播的影响。

## 数据来源

区域数据,戴口罩率,疫苗接种率等数据由用户自己上传。

## 可视化看板 🔥🔥🔥

 <table> 
   <thead> 
     <tr> 
       <th width="25%">系统界面</th> 
       <th width="25%">初始化模块</th> 
       <th width="25%">agent人数模块</th> 
       <th width="25%">echart图标模块</th> 
     </tr> 
   </thead> 
   <tbody> 
     <tr> 
       <td> 
         <img 
           src="https://s2.loli.net/2023/11/24/GZ1pctaHzPVTRom.jpg"
         /> 
       </td> 
       <td> 
         <img 
           src="https://s2.loli.net/2023/11/15/1GDAZEevCV5fj7T.png"
         /> 
       </td> 
       <td>
         <img 
           src="https://s2.loli.net/2023/11/15/rEs4Ybq8h7KMNSn.png"
         /> </td>
           <td>
         <img 
           src="https://s2.loli.net/2023/11/24/tILDladNu1FEh8Y.png"
         /> </td>
     </tr>

   </tbody> 
 </table>

- **系统界面**: 可以让大家对多智能体项目有一个基本的了解。

- **初始化模块**: 该模块可以初始化 agent，录入此次模拟的戴口罩率、疫苗接种率、医疗条件等信息。

- **agent 人数模块**：该模块可以显示模拟过程中的每种 agent 的人数信息，以及 agent 的运动快慢等信息。

- **echart 图标模块**: 该模块通过图表的形式，动态展示了可视化仿真的全过程，以及每种感染人数如何变化的详细信息。

## 参与贡献

如果你初来乍到或对 Git/gitee 的基本操作不熟悉，请阅读[Git 官方文档](https://git-scm.com/docs/)。

### 环境需求

1. node >= 16.14


### 环境搭建

如果你对node.js的环境搭建不熟悉，请阅读[node.js环境搭建](https://blog.csdn.net/m0_67840539/article/details/130607034)

### 快速开始

1. git clone https://gitee.com/os-hubu/multi-agent-front-end

2. cd multi-agent-front-end

3. npm install

4. npm run serve

5. Happy hacking!

### 问题交流

我们非常欢迎您的贡献，您可以通过 [Issue](https://gitee.com/os-hubu/multi-agent-front-end/issues) 提出问题或交流。

### 项目文件树

![@P~__U8Y3_YST31FXT7_ZJL.png](https://s2.loli.net/2023/12/10/TkeoNxnJCtHAKOa.png)



