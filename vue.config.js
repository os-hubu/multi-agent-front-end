const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api1': {
        target: 'http://111.47.28.118:30886', // 配置访问的服务器地址
        // target: 'http://127.0.0.1:9998', // 配置访问的服务器地址
        pathRewrite: { '^/api1': '' }, 
        ws: true, 
        // 是否支持 webstocket, 默认是 true
        changeOrigin: true // 用于控制请求头中的 host 值, 默认是 ture
      },
      '/api2': {
        target: 'http://111.47.28.118:30885', // 配置访问的服务器地址
        // target: 'http://127.0.0.1:9999', // 配置访问的服务器地址
        pathRewrite: { '^/api2': '' },  
        ws: true, // 是否支持 webstocket, 默认是 true
        changeOrigin: true // 用于控制请求头中的 host 值, 默认是 ture
      }
    }
  }
})
